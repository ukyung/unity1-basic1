﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
	int count = 1;
	float startingPoint;
	bool shouldPrintOver20 = true;
	bool shouldPrintOver30 = true;
	SphereCollider myCollider;

	// Use this for initialization
	void Start () {
		myCollider=GetComponent<SphereCollider>();


		Debug.Log("Start");
		TestMethod();
		startingPoint = transform.position.z;
		
	
	}
	
	// Update is called once per frame
	void Update () {
		float distance;

		distance = transform.position.z - startingPoint;
		
		if(distance > 30)
		{
			if(shouldPrintOver30){
				Debug.Log("Over 30:" + distance);
				shouldPrintOver30 = false;
			}
			
		}
		else if(distance > 20)
		{
			if(shouldPrintOver20){
				Debug.Log("Over 20:" + distance);
				shouldPrintOver20 = false;
			}
			
		}

		if(Input.GetKeyDown(KeyCode.Space)){
			//GetComponent<Rigidbody>().AddForce(Vector3.up*300);
			Rigidbody ballRigid;
			ballRigid = gameObject.GetComponent<Rigidbody> ();
			ballRigid.AddForce(Vector3.up*300);


		}
		
	}
	

	void TestMethod(){
		Debug.Log("This is TestMethod");
	}
}
