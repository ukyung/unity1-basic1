﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedCoin : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if (col.gameObject.name == "Ball") {
			Destroy (gameObject);
			GameObject.Find("GameManager").SendMessage("RedCoinStart");

		}
	}



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
