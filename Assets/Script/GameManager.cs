﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public int coinCount = 0;
	public Text cointext;

	 void GetCoin(){
		//coinCount += coinCount +1
		coinCount++;
		cointext.text = coinCount + "개";

		Debug.Log ("동전 :" + coinCount);
	}


	public void RestartGame()
	{
		SceneManager.LoadScene( "BallGame");
	}

	void RedCoinStart()
	{
		DestroyObstacles ();
	}

	void DestroyObstacles()
	{
		GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
		for(int i =0; i < obstacles.Length; i++)
		{
			Destroy(obstacles[i]);
		}
	}


	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (coinCount);
	}
}
