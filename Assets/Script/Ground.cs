﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float zRotation = transform.localEulerAngles.z;
		zRotation = zRotation - Input.GetAxis ("Horizontal");
		transform.localEulerAngles = new Vector3(7.314f,-0.283f,zRotation);

		if (Input.touchCount > 0 || Input.GetMouseButton (0)) {
			Debug.Log ("mouse down:"+Input.mousePosition);
			if(Input.mousePosition.x < Screen.width/2)
			{//왼쪽클릭
				transform.localEulerAngles = new Vector3(7.314f,-0.283f,transform.localEulerAngles.z+0.3f);
			}
			else{//오른쪽 클릭
				transform.localEulerAngles = new Vector3(7.314f,-0.283f,transform.localEulerAngles.z-0.3f);
			}
		}
			
		
	}
}
